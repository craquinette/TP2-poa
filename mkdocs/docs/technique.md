# Guide technique

Description de certains points du projet.

## Analyse

Le projet a été à son terme et ressemble à ce que j'imaginais après avoir renoncé de modéliser le problème à l'aide d'un graphique. Les axes d'amélioration sont décrits précisément dans la partie [pour aller plus loin](amelioration.md).

## Conception de l'interface

Les trois objets principaux utilisés pour l'interface sont trois ImageView : une pour le robinet, une pour la baignoire et une pour la flaque. Au démarrage de l'application, l'image du robinet sera modifiée en robinet allumé et inversement à la fin de l'application. Au fur et à mesures de l'avancée du programme et de l'appel de la fonction verser(), la baignoire va se remplir de plus en plus jusqu'à atteindre le remplissage complet. En même temps, la fonction fuite() sera appelée. Cette dernière va jouer avec la transparence de la flaque (plus elle deviendra opaque, plus la quantité d'eau dans la flaque sera importante).

Le formulaire à remplir utilise également des objets de la bibliothèque javafx. Ceux-ci sont paramétrés pour éviter les saisies utilisateurs invalides.

Pour la mise en forme, j'ai utilisé des BorderPane, pour placer les objets selon des localisations définies. Je n'ai pas eu besoin d'autres objets pour faciliter la mise en forme. Des représentations plus détaillées de l'IHM sont disponibles dans le [guide d'utilisation](utilisation.md).

## Conception des objets

Deux types d'objets, répartis en deux packages différents ont été nécessaires à la conception du programme.

### Les objets métiers

Ce sont les objets modélisant les éléments physiques de l'application : la baignoire, la flaque et le trou. Ces trois objets auront des intéractions ensemble et le lien se situe dans la baignoire. C'est dans cette classe que va se situer les deux méthodes verser() et fuite() qui vont-être appelées par les classes techniques.

### Les classes techniques

Les classes techniques, que l'on appelle services, se servent des objets métiers dans le but de paramétrer les intéractions entre eux et de réaliser les actions de chacun selon des intervalles définis. C'est ici que les threads sont utilisés pour que les deux services puissent être réalisés simultanément et répondre au problème de la baignoire qui fuit.

## Choix et principes de parallélisation des tâches
 
### Diagramme d'activités

Le diagramme d'activités va représenter une version du cycle de vie de l'application, incluant les threads :

![Diagramme d'activités](img/activites.png "Diagramme d'activités")

### Description des threads

Deux threads sont utilisés simultanément dans les classes FuiteService et VerserService. Ils vont tout d'eux manipuler l'objet baignoire en faisant varier sa quantité d'eau. J'ai choisi d'appeler les deux fonctions fuite() et verser() à intervalle régulier d'une seconde chacunes. J'ai alors estimé que la fuite et le débit prennent un temps équivalent. Le parallélisme est arrêté sous deux conditions possibles que sont le remplissage de la baignoire et la flaque qui contient une quantité d'eau supérieure au volume de la baignoire.

