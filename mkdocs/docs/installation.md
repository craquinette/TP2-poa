# Guide d'installation

Utiliser l'appplication nécessite certains prérequis.

## Installation de Java

Vérifier si java est installé sur notre machine (vérifier la version de java depuis la ligne de commande) :

![Alt Text](gif/VersionJava.gif)

Installation de Java nécessaire si ce n'est pas le cas, voici le [lien](https://www.java.com/fr/download/help/windows_manual_download.html) vers un tutoriel d'installation.


## Utilisation du programme

A la réception du dossier zip, on l'extrait pour obtenir les distributions source et binaire. Le programme ne demande aucun fichier à passer en paramètre, les données étant téléchargées au lancement de l'application. Le prérequis pour lancer l'application est de se rendre en ligne de commande sur le dossier bindist\bin (à la racine du snapshot) puis de lancer la commande run sous Windows ou run.bat sous Unix.

