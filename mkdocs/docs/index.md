# TP2

Projet utilisant javafx dans le but de modéliser une baignoire qui fuit en s'appuyant sur le parallélisme.

## Pour commencer

### Pré-requis

Ce qui est requis pour commencer avec le projet du TP2...

- Java 11
- Maven

### Arborescence de la documentation

- [Présentation générale](presentation.md)
- [Manuel d'installation](installation.md)
- [Manuel d'utilisation](utilisation.md)
- [Manuel technique](technique.md)
- [Pour aller plus loin](amelioration.md)


## Fabriqué avec

Programmes/logiciels/ressources utilisés pour développer le projet :

* Eclipse
* javafx
* org.apache.commons.cli


## Auteurs
Auteur du projet : Ernesto Dillenschneider
Ressource template readme : https://gist.github.com/JulienRAVIA/1cc6589cbf880d380a5bb574baa38811


## Licence

Ce projet n'est pas sous licence.

