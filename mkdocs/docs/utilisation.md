# Guide d'utilisation

Démonstration de l'application en s'appuyant sur des exemples.

## Ajout de trou 

L'ajout de courbe se fait au moyen du panel en dessous du graphique. L'exemple montre un remplissage avec un trou qui se passe bien.

![Alt Text](gif/ajoutTrou.gif)


## Enlever des courbes du graphique 

On peut enlever des trous avec la liste déroulante à droite du graphique qui se remplit dynamiquement au fur et à mesures des ajouts de trous. Dans le cas montré, les trous sont trop conséquents et le débit pas assez élevé : le baignoire ne se remplira jamais.

![Alt Text](gif/enleverTrou.gif)


## Océan sous la baignoire

Lorsque la quantité d'eau de la flaque dépasse le volume de la baignoire, l'application s'arrête même si la baignoire peut finir de se remplir. Le gaspillage d'eau est trop important. J'adore l'eau, dans vingt ou trente ans il n'y en aura plus.

![Alt Text](gif/gaspillage.gif)


