# Présentation générale

Présentation du projet et des différents choix effectués.

## Description du projet

L'application propose une modélisation d'une baignoire qui fuit. Le volume de la baignoire, le débit du robinet, le nombre de trous de la baignoire et le débit de chacun d'eux sont paramétrables.


## Conditions du démarrage de l'application

Ce sont les choix d'implémentation du développeur pour permettre le démarrage de l'application.

### Gestion des insertions

Les insertions requises pour le démarrage de l'application sont uniquement des entiers dont certains sont forcément positifs. Des vérifications sont alors effectuées pour chaque saisie de l'utilisateur au moyen de pattern. 

### Limite du nombre de trous

Le nombre de trous et leurs débits n'est pas limité. Aussi, il est possible d'avoir un débit total de trou très élevé. Ce choix a été fait pour laisser de la marge de manoeuvre à l'utilisateur.


## Arrêt de l'application

Ce sont les choix du développeur pour préciser quand s'arrête l'application.

### Baignoire remplie

La baignoire est remplie lorsque la quantité d'eau égale le volume de celle-ci. C'est calculé à chaque fois qu'une unité est versée dans la baignoire à l'aide d'un ratio. Un texte nous informe de la fin du remplissage et on peut s'en apercevoir par la couleur de la baignoire qui vire petit à petit au bleu.


### Trop d'eau gaspillée

Le robinet s'éteint lorsque la flaque est devenue trop importante. C'est le cas lorsque la quantité d'eau de la flaque dépasse le volume de la baignoire. Suffisament d'eau a été perdue, cela met fin à l'application. Ceci est caractérisé par l'apparition d'un texte et par l'opacité de la flaque dont la couleur devient de plus en plus bleue.