# Pour aller plus loin

Description des axes d'amélioration potentiels du projet.

## Pertinence des widgets

Initialement, les widgets que je pensais utiliser étaient des charts avec des axes montrant l'eau gaspillée et le remplissage de la baignoire au fur et à mesure. J'ai préféré opter pour des ImageView qui donnent un aspect plus ludique à l'application. Après tests, il s'est avéré que l'utilisation de ces dernières en jouant avec l'opacité et en redessinant une partie des pixels était réalisable avec le temps d 'une seconde laissé entre chaque versement et fuite.

## Esthétique de l'application

Le placement des widgets et l'agencement des images de l'application me semble améliorable avec notamment l'image du robinet qui est légèrement déformé lors de l'arrêt de l'application. J'ai choisi de remplir la baignoire en diagonale car dans l'univers de l'application, la gravité est différente de celle que nous connaissons.