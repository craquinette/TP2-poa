package Controllers;

import java.io.File;
import java.util.function.UnaryOperator;

import Objects.Baignoire;
import Objects.Flaque;
import Objects.Trou;
import Services.FuiteService;
import Services.VerserService;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class App extends Application{
	
	private final File file_robinet = new File("..\\..\\samples/robinet.png");
	public static final String folderRegister = "..\\..\\samples/";
	private final String url_robinet = file_robinet.toURI().toString();
	private final File file_robinet_allume = new File("..\\..\\samples/robinet-allume.png");
	private final String url_robinet_allume = file_robinet_allume.toURI().toString();
	private final File file_baignoire = new File("..\\..\\samples/baignoire.png");
	private final String url_baignoire = file_baignoire.toURI().toString();
	private final File file_flaque = new File("..\\..\\samples/flaque.png");
	//private final File file_flaque = new File("./samples/flaque.png");
	private final String url_flaque = file_flaque.toURI().toString(); 
	
	private Image image_robinet = new Image(url_robinet, true);
	private ImageView image_vue_robinet = new ImageView(image_robinet);
	private Image image_baignoire = new Image(url_baignoire, true);
	private ImageView image_vue_baignoire = new ImageView(image_baignoire);
	private Image image_flaque = new Image(url_flaque, true);
	private ImageView image_vue_flaque = new ImageView(image_flaque);
	
	private Baignoire baignoire;
	private Flaque flaque;
	private VerserService verser;
	private FuiteService fuite;
	
	private Button btnDemarrer;
	private ObservableList<Trou> debitsTrou = FXCollections.observableArrayList();
	private ChoiceBox<Trou> cbTrou;
	private Button btnRetirerTrou;
	private Label lblTrous;
	private TextField txtTrou;
	private Button btnAjoutTrou;
	private Label lblResumeTrous;
	private Label lblVolume;
	private TextField txtVolume;
	private Label lblDebit;
	private TextField txtDebit;
	private Label lblErreur;
	private Label lblInfos;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Exercice : TP2");
		BorderPane root = new BorderPane();
		
		//Composant de la partie supérieure
		BorderPane topPane = new BorderPane();
		
		image_vue_robinet.setFitHeight(150);
		image_vue_robinet.setFitWidth(100);
		image_vue_baignoire.setFitHeight(150);
		image_vue_baignoire.setFitWidth(500);
		image_vue_flaque.setFitHeight(100);
		image_vue_flaque.setFitWidth(300);
		image_vue_flaque.setOpacity(0);
		
		topPane.setTop(image_vue_robinet);
		BorderPane.setAlignment(image_vue_robinet, Pos.TOP_RIGHT);
		topPane.setCenter(image_vue_baignoire);
		topPane.setBottom(image_vue_flaque);
			
		//Composants de la droite de la page
		BorderPane rightPane = new BorderPane();
		lblTrous = new Label("Ajouter des trous");
		UnaryOperator<Change> filterNumber = change -> {
		    String text = change.getText();
		    if (text.matches("[0-9]*")) return change;
		    return null;
		};
		TextFormatter<String> numberOnlyTrou = new TextFormatter<>(filterNumber);
		txtTrou = new TextField();
		txtTrou.setTextFormatter(numberOnlyTrou);
		btnAjoutTrou = new Button("+"); 
		
		lblResumeTrous = new Label();
		lblResumeTrous.setVisible(false);
		btnAjoutTrou.setOnAction((e) -> {
			if(!txtTrou.getText().equals("") && debitsTrou.size()<=9) {
				debitsTrou.add(new Trou(Double.parseDouble(txtTrou.getText())));
				lblResumeTrous.setText(lblResumeTrous.getText()+"\n"+"Trou "+debitsTrou.size()+" avec un débit de "+txtTrou.getText());
				lblResumeTrous.setVisible(true);
				txtTrou.setText("");
				if (!debitsTrou.isEmpty())  cbTrou.setValue(debitsTrou.get(0));
			}
		});
		
		BorderPane ajoutTrouPane = new BorderPane();	
		ajoutTrouPane.setTop(lblTrous);
		BorderPane.setAlignment(lblTrous, Pos.CENTER);
		ajoutTrouPane.setRight(btnAjoutTrou);
		ajoutTrouPane.setCenter(txtTrou);
		
		BorderPane enleverTrouPane = new BorderPane();
		cbTrou = new ChoiceBox(debitsTrou);
		btnRetirerTrou = new Button("Enlever trou");
		btnRetirerTrou.setOnAction((e) -> {
			if(cbTrou.getValue() != null) {
				debitsTrou.remove(cbTrou.getValue());
				lblResumeTrous.setText("");
				for(int i=1;i<=debitsTrou.size();i++) lblResumeTrous.setText("Trou "+i+" avec un débit de "+debitsTrou.get(i-1).getDebit()+"\n");
				if(debitsTrou.size() == 0) lblResumeTrous.setVisible(false);
				if (!debitsTrou.isEmpty()) cbTrou.setValue(debitsTrou.get(0));
			}
			
		});
		lblInfos=new Label();
		lblInfos.setVisible(false);
		enleverTrouPane.setTop(lblInfos);
		enleverTrouPane.setLeft(cbTrou);
		enleverTrouPane.setRight(btnRetirerTrou);
		
		rightPane.setTop(ajoutTrouPane);
		rightPane.setCenter(lblResumeTrous);
		rightPane.setBottom(enleverTrouPane);
		BorderPane.setAlignment(lblResumeTrous, Pos.TOP_LEFT);
			
		//Composants du bas de la page
		lblErreur = new Label();
		lblErreur.setVisible(false);
		lblVolume = new Label("Volume de la baignoire");
		TextFormatter<String> numberOnlyVolume = new TextFormatter<>(filterNumber);
		txtVolume = new TextField();
		txtVolume.setTextFormatter(numberOnlyVolume);
		BorderPane panVolume = new BorderPane();
		panVolume.setTop(lblVolume);
		panVolume.setBottom(txtVolume);
		
		lblDebit = new Label("Débit du robinet");
		TextFormatter<String> numberOnlyDebit = new TextFormatter<>(filterNumber);
		txtDebit = new TextField();
		txtDebit.setTextFormatter(numberOnlyDebit);
		BorderPane panDebit = new BorderPane();
		panDebit.setTop(lblDebit);
		panDebit.setBottom(txtDebit);
		
		btnDemarrer = new Button("Démarrer");
		
		BorderPane bottomPane = new BorderPane();
		bottomPane.setTop(lblErreur);
		bottomPane.setLeft(panVolume);
		bottomPane.setCenter(panDebit);
		bottomPane.setRight(btnDemarrer);
		BorderPane.setAlignment(btnDemarrer, Pos.BOTTOM_CENTER);
		
		btnDemarrer.setOnAction((e) -> {
			lblErreur.setVisible(false);
			if(!txtVolume.getText().equals("") && Integer.parseInt(txtVolume.getText())!=0 && !txtDebit.getText().equals("")) {
				baignoire = new Baignoire(Double.parseDouble(txtVolume.getText()));
				flaque = new Flaque(baignoire);
				
				baignoire.setDebit_robinet(Double.parseDouble(txtDebit.getText()));
				
				for(Trou t : debitsTrou) {
					System.out.println(t);
					baignoire.ajouterTrou(t); 
				}
				enableAllComposants(true);
				Image image_robinet_allume = new Image(url_robinet_allume, true);
				image_vue_robinet.setImage(image_robinet_allume);
	
				verser = new VerserService(baignoire);
				fuite = new FuiteService(flaque);
				initialize();
				try {		
					demarrer();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
			else {
				lblErreur.setText("Le volume et le débit se doivent d'être renseignés. Le volume doit-être supérieur à 0.");
				lblErreur.setVisible(true);
			}
		});
		root.setBottom(bottomPane);
		BorderPane.setAlignment(bottomPane, Pos.BOTTOM_CENTER);
		root.setRight(rightPane);
		root.setCenter(topPane);
		
		enableAllComposants(false);
		Scene scene = new Scene(root, 750, 500);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void initialize() {
		lblInfos.setVisible(false);
		verser.setOnSucceeded((WorkerStateEvent event) -> {
			remplirBaignoire();
			if(baignoire.getVolume()<=baignoire.getQuantite_eau()) {
				lblInfos.setText("Baignoire remplie");
				lblInfos.setVisible(true);
				verser.cancel();
				fuite.cancel();
				image_vue_robinet.setImage(image_robinet);
				enableAllComposants(false);
			}
		});
		fuite.setOnSucceeded((WorkerStateEvent event) -> {			
			remplirBaignoire();
			image_vue_flaque.setOpacity((double)flaque.getQuantite_eau()/baignoire.getVolume());
			if(baignoire.getVolume()<=flaque.getQuantite_eau()) {
				lblInfos.setText("Vous avez gaspillé suffisamment d'eau");
				lblInfos.setVisible(true);
				verser.cancel();
				fuite.cancel();
				image_vue_robinet.setImage(image_robinet);
				enableAllComposants(false);
			}
		});
	}
	
	public void remplirBaignoire() {
		PixelReader pixelReader = image_baignoire.getPixelReader();
		WritableImage wImage = new WritableImage((int)image_baignoire.getWidth(),(int)image_baignoire.getHeight());
		PixelWriter pixelWriter = wImage.getPixelWriter();		
		int pixelStop = (int)(baignoire.getQuantite_eau()*(image_baignoire.getHeight()*image_baignoire.getWidth())/baignoire.getVolume());
		for (int y = (int) image_baignoire.getHeight()-1; y >= 0; y--){
			for (int x = 0; x < image_baignoire.getWidth(); x++){
				Color color = pixelReader.getColor(x, y);
				if ((x*(image_baignoire.getHeight()-1-y))<pixelStop && color.getRed()>0.99 && color.getBlue()>0.99 && color.getGreen()>0.99) color = Color.rgb(0, 0, 255);
				else if ((x*(image_baignoire.getHeight()-1-y))>pixelStop && color.getRed()==0 && color.getBlue()==0 && color.getGreen()==255) color = Color.WHITE;
				pixelWriter.setColor(x,y,color);
			}
		}
		image_vue_baignoire.setImage(wImage);
	}
	
	public void demarrer() throws InterruptedException{
		verser.start();
		Thread.sleep(500);
		fuite.start();
	}
	
	public void enableAllComposants(boolean actif){
		btnAjoutTrou.setDisable(actif);
		btnRetirerTrou.setDisable(actif);
		btnDemarrer.setDisable(actif);
	}
}