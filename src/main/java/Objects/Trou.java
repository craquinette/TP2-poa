package Objects;

public class Trou {

	private double debit;
	
	public Trou(double debit) {
		this.debit=debit;
	}

	public double getDebit() {
		return debit;
	}

	@Override
	public String toString() {
		return "Trou avec un débit de " + debit;
	}
	
}
