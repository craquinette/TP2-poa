package Objects;

import java.time.LocalTime;

public class Flaque {

	private double quantite_eau;
	private Baignoire baignoire;
	
	public Flaque(Baignoire baignoire) {
		quantite_eau=0;
		this.baignoire=baignoire;
	}

	public double getQuantite_eau() {
		return quantite_eau;
	}

	public void setQuantite_eau(double quantite_eau) {
		this.quantite_eau = quantite_eau;
	}
	
	public double accumulerEau() {
		double fuite=0.0d;
		if (baignoire.debitTotal()<=0.0d) {
			return fuite;
		}
		synchronized (baignoire) {
			quantite_eau+=baignoire.debitTotal();
			fuite= baignoire.fuite(baignoire.debitTotal());
		}
		System.out.println(LocalTime.now());
		System.out.println(":Flaque: " + quantite_eau  + " litres dans la baignoire "+baignoire.getQuantite_eau());
		//System.out.printf(":Flaque:%d/Temps:%dReste:%d litres dans la baignoire%n",quantite_eau,baignoire.getTemps_ecoule(),baignoire.getVolume());
		return fuite;
	}
}
