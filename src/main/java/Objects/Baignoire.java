package Objects;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Baignoire {

	private static final Logger LOG=Logger.getLogger(Baignoire.class.getName());
			
	private List<Trou> trous;
	
	private double quantite_eau;
	private double volume;
	private double debit_robinet;
	private int temps_ecoule;
	
	public Baignoire(double volume) {
		temps_ecoule=0;
		this.volume=volume;
		quantite_eau=0; 
		debit_robinet=0;
		trous = new ArrayList<Trou>();
	
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getDebit_robinet() {
		return debit_robinet;
	}

	public void setDebit_robinet(double debit_robinet) {
		this.debit_robinet = debit_robinet;
	}
	
	public void ajouterTrou(Trou trou) {
		trous.add(trou);
	}
	
	public double debitTotal() {
		double debit = 0.0d;
		for(Trou t : trous) {
			debit+=t.getDebit();
		}
		return debit;
	}
	
	public int getTemps_ecoule() {
		return temps_ecoule;
	}

	public double getQuantite_eau() {
		return quantite_eau;
	}

	public void incrementerTemps() {
		temps_ecoule++;
	}
	
	public double verser() {
		if (volume < quantite_eau) {
			System.out.println("Baignoire remplie");
			System.exit(1);
		}
		try {
			Thread.sleep(20);
		} catch (InterruptedException e) {
			LOG.warning(e.getMessage());
		}
		double v=debitTotal();
		synchronized (this) {
			this.gererQuantite(debit_robinet);
		}
		System.out.println(LocalTime.now());
		System.out.println("Baignoire : "+volume+ " Litres remplie :" + quantite_eau);
		//System.out.printf(": Baignoire%d/Litres:%d%n", volume, quantite_eau);
		return v;
	}

	private void gererQuantite(double debitTotal) {
		System.out.println("avant qte "+quantite_eau);
		quantite_eau+=debitTotal;
		System.out.println("apres qte " +quantite_eau);
	}

	public double fuite(double debitTotal) {
		if(debitTotal>quantite_eau) {
			System.out.println("Baignoire vide");
			//System.exit(1);
		}
		this.gererQuantite(-debitTotal());
		return debitTotal;
	}
	
	
}
