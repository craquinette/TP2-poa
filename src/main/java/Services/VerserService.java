package Services;

import Objects.Baignoire;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;

public class VerserService extends ScheduledService<Double>{
	
	private Baignoire baignoire;
	
	public VerserService(Baignoire baignoire){
		super();
		this.baignoire=baignoire;
		this.setPeriod(Duration.seconds(1));
		
	}

	@Override
	protected Task<Double> createTask() {
		return new Task<Double>() {
			protected Double call() {
				return baignoire.verser();
			};
		};
	}

}
