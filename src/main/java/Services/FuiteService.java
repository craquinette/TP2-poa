package Services;

import Objects.Flaque;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;

public class FuiteService extends ScheduledService<Double>{

	private Flaque flaque;
	
	public FuiteService(Flaque flaque){
		super();
		this.flaque=flaque;
		this.setPeriod(Duration.seconds(1));
		
	}

	@Override
	protected Task<Double> createTask() {
		return new Task<Double>() {
			protected Double call() {
				return flaque.accumulerEau();
			};
		};
	}


}
